---
home: true
heroImage: /1.jpg
# actionText: 快速上手 →
# actionLink: /zh/guide/
features:
- title: 自律
  details: 其身正，不令而行；其身不正，虽令不从
- title: 分享
  details: 紫金山下水长流，尝记当年此共游
- title: 坚持
  details: 长风破浪会有时，直挂云帆济沧海。
footer: 记录自己成长的空间
