# 常用的页面布局方式

## 左右两栏布局

基本的html,css代码如下:

```html
<div class="box">
    <div class="left"></div>
    <div class="right"></div>
</div>
```

```css
html,body{
    width: 100%;
    height: 100%;
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}
```

1. flex布局,简洁、易理解

```css
.box{
    width: 100%;
    height: 100%;
    display: flex;
}
.left{
    width: 200px;
    background: red;
}
.right{
    flex: 1;
    background: yellowgreen;
}
```

2. float布局<br/><br/>
使用float布局的优点是,兼容性比较好,但是也需要注意浮动带来的一些负面效果
```css
.box{
    width: 100%;
    height: 100%;
}
.left{
    width: 200px;
    height: 100%;
    background: red;
    float: left;
}
.right{
    padding-left: 200px;
    height: 100%;
    background: yellowgreen;
}
```

3. 使用绝对定位,position+margin
```css
.box{
    width: 100%;
    height: 100%;
    position: relative;
}
.left{
    width: 200px;
    height: 100%;
    background: red;
    position:absolute;
    left: 0;
}
.right{
    height: 100%;
    background: yellowgreen;
    margin-left: 200px;
}
```

## 上下两栏布局

```html
<div class="box">
    <div class="top"></div>
    <div class="bottom"></div>
</div>
```

```css
html,body{
    width: 100%;
    height: 100%;
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    overflow: hidden;
}
```

1. 绝对定位

```css
.box{
    width: 100%;
    height: 100%;
}
.top{
    position: absolute;
    top:0;
    left: 0;
    right: 0;
    height: 50px;
    background: red;
}
.bottom{
    position: absolute;
    width: 100%;
    height: 100%;
    top:50px;
    left: 0;
    right: 0;
    background: greenyellow;
}
```
