module.exports = {
    title: '小小部落',
    description: 'Just playing around',
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { 
                text: 'JavaScript',
                items:[
                    {text:'ES6',link:'/JavaScript/'},
                    {text:'VUE',link:'/JavaScript/vue'},
                    {text:'React',link:'/JavaScript/react'},
                ]
            },
            { text: 'CSS', link: '/CSS/' },
        ],
        sidebar: 'auto'
    },
    markdown: {
        lineNumbers: true
    }
  }